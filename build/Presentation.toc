\beamer@endinputifotherversion {3.36pt}
\beamer@sectionintoc {1}{General}{2}{0}{1}
\beamer@subsectionintoc {1}{1}{Structure}{2}{0}{1}
\beamer@sectionintoc {2}{Used}{3}{0}{2}
\beamer@subsectionintoc {2}{1}{Observables}{3}{0}{2}
\beamer@sectionintoc {3}{The}{4}{0}{3}
\beamer@subsectionintoc {3}{1}{Model}{4}{0}{3}
\beamer@sectionintoc {4}{Measurements of}{5}{0}{4}
\beamer@subsectionintoc {4}{1}{the observables}{5}{0}{4}
\beamer@sectionintoc {5}{Model}{6}{0}{5}
\beamer@subsectionintoc {5}{1}{Implementation}{6}{0}{5}
\beamer@sectionintoc {6}{The}{9}{0}{6}
\beamer@subsectionintoc {6}{1}{Results}{9}{0}{6}
\beamer@sectionintoc {7}{The}{12}{0}{7}
\beamer@subsectionintoc {7}{1}{End}{12}{0}{7}
