import os

def areMF():
	workingList = os.listdir()
	listlen = len(workingList)
	ismffile = False
	for i in range(listlen):			
		if ".mf" in workingList[i]:
			ismffile = True
	return ismffile

def arebib():
	workingList = os.listdir()
	listlen = len(workingList)
	isbibfile = False
	for i in range(listlen):			
		if ".bib" in workingList[i]:
			istbibfile = True
	return isbibfile


def getMFFiles():
	workingList = os.listdir()
	listlen = len(workingList)
	mffile = []
	for i in range(listlen):			
		if ".mf" in workingList[i]:
			mffile.append(i)
	return mffile

for i in range(len(os.listdir())):
	if "Presentation.tex" in (os.listdir())[i]:
		os.system("rm *.mf *.*pk *.*gf")

		try:
			os.system("pdflatex -i Presentation.tex --interaction=nonstopmode")
		except:
			j=1
		else:
			if(areMF()):
				os.system("ls | grep .mf | parallel mf")
				os.system("pdflatex -i Presentation.tex --interaction=nonstopmode")
				os.system("rm *.mf *.*pk *.*gf *.log *.t1 *.tfm")
				
		print('Fertig :)')

