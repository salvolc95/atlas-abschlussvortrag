... Blue->FixInp(): Fixed to InpEst =  2, InpUnc =  1, InpObs = 1
... Blue->SetFixedInp(l): Input fixed for solving! 
... Blue->SetIsSolved(l): Input was solved! 

... Blue->PrintChiPro(): ChiQua = 1.011 for NDof =  1 Probability = 31.46 % 
... Blue->PrintEst:                ValueDaStat  
... Blue->PrintEst( 0) ATLAS:  27.200 +- 1.931
... Blue->PrintEst( 1) CMS:  23.400 +- 1.895
... Blue->PrintRho: The correlation matrix of the input 

2x2 matrix is as follows

     |      0    |      1    |
-------------------------------
   0 |          1       -0.95 
   1 |      -0.95           1 

... Blue->PrintResult: Linear combination of estimates for observables 
               xi =     ATLAS       CMS    
Sigma = 25.282 = +0.495 * x0 + +0.505 * x1 

... Blue->PrintResult: Breakdown of uncertainties 
 Uncert =                 DaStat  
Sigma = 25.282 +- 0.302 

... Blue->PrintResult: Breakdown in stat (k==0)
 Result =   Value (+-  stat) = +-  full 
Sigma = 25.282 (+- 0.302) = +- 0.302
